function InDevelopment() {
  return (
    <div className="w-full h-full flex justify-center items-center">
      <h1 className="text-gray-600 text-[48px] text-bold">
        Esta sessão está em desenvolvimento
      </h1>
    </div>
  );
}

export default InDevelopment;
