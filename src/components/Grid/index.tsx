import * as React from "react";
import Box from "@mui/material/Box";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { ClientContext } from "@/Context/ClientsContext";

const columns: GridColDef[] = [
  {
    field: "FirstName",
    headerName: "First name",
    width: 150,
    editable: true,
  },
  {
    field: "LastName",
    headerName: "Last name",
    width: 150,
    editable: true,
  },
  {
    field: "Age",
    headerName: "Age",
    type: "number",
    width: 110,
    editable: true,
  },
  {
    field: "Gender",
    headerName: "Gender",
    type: "number",
    width: 150,
    editable: true,
  },
  {
    field: "Country",
    headerName: "Country",
    width: 150,
    editable: true,
  },
];

export default function DataGridDemo() {
  const { allClients } = React.useContext(ClientContext);
  const rows = allClients.map((e: any, index) => ({
    id: index,
    FirstName: String(e.name.first),
    LastName: String(e.name.last),
    Age: String(e.registered.age),
    Gender: String(e.gender),
    Country: String(e.location.country),
  }));
  console.log(rows);

  return (
    <Box sx={{ height: 350, width: "100%" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: {
              pageSize: 4,
            },
          },
        }}
        pageSizeOptions={[4]}
        disableRowSelectionOnClick
      />
    </Box>
  );
}
