import { Bar } from "react-chartjs-2";
import { Chart, CategoryScale, LinearScale, BarElement } from "chart.js";
import { useContext } from "react";

import { ClientContext } from "@/Context/ClientsContext";

function Bars() {
  const { clients, country } = useContext(ClientContext);
  Chart.register(CategoryScale, LinearScale, BarElement);
  const data = {
    labels: country,
    datasets: [
      {
        data: clients,
        backgroundColor: ["gray"],
      },
    ],
  };
  return (
    <div className="flex md:w-full flex-col">
      <h2 className="text-gray-600 font-bold p-5">
        Possiveis clientes em cada país
      </h2>
      <div className="p-5">
        <Bar
          data={data}
          width={400}
          height={400}
          options={{
            maintainAspectRatio: false,
          }}
        />
      </div>
    </div>
  );
}

export default Bars;
