import Aside from "@/components/Aside";
import Dashboard from "@/components/Dashboard";
import InDevelopment from "@/components/InDevelopment";
import { Inter } from "next/font/google";
import Head from "next/head";
import { useState } from "react";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const [session, setSession] = useState(true);
  return (
    <main className={`w-full h-screen ${inter.className} flex`}>
      <Head>
        <title>Desafio NG7</title>
      </Head>

      <Aside setSession={setSession} />

      {session ? <Dashboard /> : <InDevelopment />}
    </main>
  );
}
