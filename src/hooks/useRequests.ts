import { api } from "@/services/apiRandomUsers";

export const useRequest = () => {
  const getData = async () => {
    return await api.get("");
  };

  return { getData };
};
