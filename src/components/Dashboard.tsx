import Header from "./Header";
import Bars from "./Bars";
import Circles from "./Circles";
import DataGridDemo from "./Grid";

function Dashboard() {
  return (
    <div className="w-full flex flex-col items-center ">
      <Header />
      <div className="flex flex-col w-full">
        <div className="flex flex-col gap-5 md:flex-row">
          <Bars />
          <Circles />
        </div>
        <DataGridDemo />
      </div>
    </div>
  );
}

export default Dashboard;
