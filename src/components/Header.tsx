import { HiUserGroup } from "react-icons/hi";
import { FaMale, FaFemale } from "react-icons/fa";
import { useContext } from "react";
import { ClientContext } from "@/Context/ClientsContext";

function Header() {
  const { allClients, womans, mans, setAge } = useContext(ClientContext);
  return (
    <header className="w-[100%] h-[100px] flex justify-between items-center p-5 shadow-md">
      <h1 className="text-gray-600 text-2xl font-bold">NG7 DashBoard</h1>
      <div className="md:w-[50%] text-gray-600 flex gap-5">
        <div className="flex gap-3 justify-center items-center">
          <HiUserGroup size={48} />
          <div>
            <h2 className="font-bold">Clientes</h2>
            <p>{allClients.length}</p>
          </div>
        </div>
        <div className="md:flex gap-3 justify-center items-center hidden">
          <FaMale size={48} />
          <div>
            <h2 className="font-bold">Homens</h2>
            <p>{mans}</p>
          </div>
        </div>
        <div className="md:flex gap-3 justify-center items-center hidden">
          <FaFemale size={48} />
          <div>
            <h2 className="font-bold">Mulheres</h2>
            <p>{womans}</p>
          </div>
        </div>
        <select
          onChange={(e) => {
            console.log(e);
            setAge(e.target.value);
          }}
          className="w-[200px] p-3 bg-gray-200 rounded-2xl hidden md:flex"
          name=""
          id=""
        >
          <option value="true">Adults</option>
          <option value="false">Underage</option>
        </select>
      </div>
    </header>
  );
}

export default Header;
