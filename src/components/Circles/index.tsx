import { Doughnut } from "react-chartjs-2";
import { Chart, ArcElement } from "chart.js";
import { useContext } from "react";

import { ClientContext } from "@/Context/ClientsContext";

function Circles() {
  const { clients, country, mans, womans, allClients, adultWomans } =
    useContext(ClientContext);
  Chart.register(ArcElement);

  const data = {
    labels: ["Homens", "Mulheres"],
    datasets: [
      {
        data: [mans, womans],

        backgroundColor: ["#979797", "#444444"],
        hoverBackgroundColor: ["#818181", "#2c2c2c"],
        hoverOffset: 4,
      },
    ],
  };
  return (
    <div className="flex flex-col w-full md:w-[40%] md:flex-row">
      <div className="flex flex-col items-center gap-3 p-5 justify-center">
        <h2 className="text-gray-600">
          Homens e Mulheres com menos de 18 anos
        </h2>
        <div className="">
          <Doughnut
            data={data}
            options={{
              plugins: {
                legend: {
                  display: true,
                  position: "bottom",
                },
              },
              scales: {
                x: {
                  ticks: {
                    display: false,
                  },
                },
              },
            }}
            width={320}
            height={320}
          />
        </div>
      </div>
      <div className="flex flex-col items-center gap-3 p-5 justify-center">
        <h2 className="text-gray-600">Mulheres com mais de 18 anos</h2>
        <div className="">
          <Doughnut
            data={{
              labels: ["Underage", "Adults"],
              datasets: [
                {
                  data: [adultWomans - womans, adultWomans],

                  backgroundColor: ["#979797", "#d8b3db"],
                  hoverBackgroundColor: ["#818181", "#c896cc"],
                  hoverOffset: 4,
                },
              ],
            }}
            options={{
              plugins: {
                legend: {
                  display: true,
                  position: "bottom",
                },
              },
              scales: {
                x: {
                  ticks: {
                    display: false,
                  },
                },
              },
            }}
            width={320}
            height={320}
          />
        </div>
      </div>
    </div>
  );
}

export default Circles;
