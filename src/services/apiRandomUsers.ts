import axios from "axios";

export const api = axios.create({
  baseURL: `https://randomuser.me/api/?results=${
    Math.floor(Math.random() * (1000 - 500 + 1)) + 500
  }`,
  timeout: 5000,
});
