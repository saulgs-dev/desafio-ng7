/* eslint-disable react-hooks/exhaustive-deps */
import { useRequest } from "@/hooks/useRequests";
import { createContext, ReactNode, useEffect, useState } from "react";

export const ClientContext = createContext({} as iClient);

interface iClient {
  clients: string[];
  setClients: React.Dispatch<React.SetStateAction<never[] | any>>;
  setAge: React.Dispatch<React.SetStateAction<never[] | any>>;
  country: string[];
  allClients: object[];
  womans: number;
  mans: number;
  adultWomans: number;
}
interface iNode {
  children: ReactNode;
}

export const ClientProvider = ({ children }: iNode) => {
  const [clients, setClients] = useState([]);
  const [country, setCountry] = useState<string[]>([]);
  const [allClients, setAllClients] = useState([]);
  const [womans, setWomans] = useState(0);
  const [mans, setMans] = useState(0);
  const [age, setAge] = useState(true);
  const [adultWomans, setAdultWomans] = useState(0);

  const { getData } = useRequest();
  useEffect(() => {
    const response = async () => {
      const data = (await getData()).data.results;
      setAllClients(data);

      let man = 0;
      let woman = 0;
      let woman18 = 0;

      const contagemNacionalidades: any = {};
      for (const usuario of data) {
        if (usuario.gender !== "male" && usuario.registered.age > 18) {
          woman18++;
        }
        if (age) {
          const { location }: any = usuario;
          contagemNacionalidades[location.country] =
            (contagemNacionalidades[location.country] || 0) + 1;
          if (usuario.gender !== "male" && usuario.registered.age > 18) {
            woman18++;
          }

          usuario.gender === "male" ? man++ : woman++;
        } else if (!age && usuario.registered.age < 18) {
          const { location }: any = usuario;
          contagemNacionalidades[location.country] =
            (contagemNacionalidades[location.country] || 0) + 1;
          console.log(usuario);

          usuario.gender === "male" ? man++ : woman++;
        }
      }

      setMans(man);
      setWomans(woman);
      setAdultWomans(woman18);

      setCountry(Object.keys(contagemNacionalidades));
      setClients(Object.values(contagemNacionalidades));
    };

    response();
  }, [age]);

  return (
    <ClientContext.Provider
      value={{
        clients,
        setClients,
        country,
        allClients,
        womans,
        mans,
        setAge,
        adultWomans,
      }}
    >
      {children}
    </ClientContext.Provider>
  );
};
