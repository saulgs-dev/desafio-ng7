import {
  BsFillClipboard2DataFill,
  BsFillGearFill,
  BsFillHouseDoorFill,
  BsPersonCircle,
  BsBellFill,
  BsFillBarChartLineFill,
} from "react-icons/bs";

function Aside({ setSession }: any) {
  return (
    <aside className="h-full w-[7%] bg-gray-500  flex-col items-center justify-center gap-20 hidden md:flex">
      <div className="flex flex-col w-full h-[35%] gap-7 mt-5">
        <button
          onClick={() => setSession(true)}
          className="w-[100%] h-[100px] hover:bg-gray-600 flex justify-center items-center flex-col gap-2"
        >
          <BsFillHouseDoorFill size={32} />
          <p>Dashboard</p>
        </button>
        <button
          onClick={() => setSession(false)}
          className="w-[100%] h-[100px] hover:bg-gray-600 flex justify-center items-center flex-col gap-2"
        >
          <BsFillClipboard2DataFill size={32} />
          <p>Extratos</p>
        </button>
        <button
          onClick={() => setSession(false)}
          className="w-[100%] h-[100px] hover:bg-gray-600 flex justify-center items-center flex-col gap-2"
        >
          <BsFillBarChartLineFill size={32} />
          <p>Notas Fiscais</p>
        </button>
      </div>
      <div className="h-[60%] flex-col flex gap-10">
        <BsPersonCircle size={32} />
        <BsBellFill size={32} />
        <BsFillGearFill size={32} />
      </div>
    </aside>
  );
}

export default Aside;
